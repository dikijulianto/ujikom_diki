<?php
include ('cek.php');
include ('koneksi.php');
?>
<!DOCTYPE html>
<html class="no-js">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>inventaris Sekolah</title>
    <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
        rel='stylesheet'>
</head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i>
                                <?php
								echo $_SESSION['petugas'];

                              ?>
                                </a>

                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>

                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
<div class="container-fluid">
            <div class="row-fluid">
            <?php

            if ($_SESSION['id_level']==1){

                      echo'<div class="span3" id="sidebar">
                          <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                              <li>
                                  <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                              </li>
                              <li>
                                  <a href="inventaris.php"><i class="icon-chevron-right"></i> inventaris</a>
                              </li>
                              <li>
                                  <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman </a>
                              </li>
                              <li>
                                  <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian </a>
                              </li>
                              <li>
                                  <a href="laporan.php"><i class="icon-chevron-right"></i> Laporan</a>
                              </li>
                              <br>
                              <li>
                                  <a href="ruang.php"><i class="icon-chevron-right"></i> ruang</a>
                              </li>
                              <li>
                                  <a href="pegawai.php"><i class="icon-chevron-right"></i> Pegawai</a>
                              </li>
                              <li>
                                  <a href="jenis.php"><i class="icon-chevron-right"></i> jenis</a>
                              </li>
                              <li>
                                  <a href="logout.php"><i class="icon-chevron-right"></i> Keluar</a>
                              </li>

                          </ul>
                      </div>';
			}
			elseif ($_SESSION['id_level']==2){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>

                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
                        <li>
                            <a href="logout.php"><i class="icon-chevron-right"></i> Keluar</a>
                        </li>

                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==3){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="logout.php"><i class="icon-chevron-right"></i> Keluar</a>
                        </li>


                    </ul>
                </div>';
			}
            ?>

               <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Di Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="index.php">Dashboard</a> <span class="divider">/</span>
	                                    </li>
	                                    <li class="active">Peminjaman</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>


 <div class="right_col" role="main">

     <div class="">
         <div class="page-title">


             <div class="title_right">
                 <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                     <div class="input-group">

                         <span class="input-group-btn">

             </span>
                     </div>
                 </div>
             </div>
         </div>
     </div>
                <div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">

                                              <div class="container">
                                                          <br>
                                                          <center>
                                                              <div class="col-md-12">
                                                                  <h1>Selamat Datang <?php echo $_SESSION['petugas']; ?></h1>
                                                                  <h4>Selamat Datang Di Website Inventaris Sekolah</h4>
                                                              </div>
                                                          </div>
                                                          </center>
                                                          <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                          <thead>

                <?php
                  include ("footer.php");
                 ?>



              </thead>
</table>
             <br>

        <div class="footer">
            <div class="container">
                <b class="copyright">
            </div>
        </div>
          <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="scripts/datatables/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        } );
    </script>
</body>
