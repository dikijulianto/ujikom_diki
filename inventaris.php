<?php
include ('cek.php');
include ('koneksi.php');
include ('cek_level.php');
?>
<!DOCTYPE html>
<html class="no-js">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>inventaris Sekolah</title>
    <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
        rel='stylesheet'>
</head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i>
                                <?php
								echo $_SESSION['petugas'];

                              ?>
                                </a>

                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>

                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
<div class="container-fluid">
            <div class="row-fluid">
            <?php

            if ($_SESSION['id_level']==1){

                      echo'<div class="span3" id="sidebar">
                          <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                              <li>
                                  <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                              </li>
                              <li>
                                  <a href="inventaris.php"><i class="icon-chevron-right"></i> inventaris</a>
                              </li>
                              <li>
                                  <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman </a>
                              </li>
                              <li>
                                  <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian </a>
                              </li>
                              <li>
                                  <a href="laporan.php"><i class="icon-chevron-right"></i> Laporan</a>
                              </li>
                              <br>
                              <li>
                                  <a href="ruang.php"><i class="icon-chevron-right"></i> ruang</a>
                              </li>
                              <li>
                                  <a href="pegawai.php"><i class="icon-chevron-right"></i> Pegawai</a>
                              </li>
                              <li>
                                  <a href="jenis.php"><i class="icon-chevron-right"></i> jenis</a>
                              </li>
                              <li>
                                  <a href="logout.php"><i class="icon-chevron-right"></i> Keluar</a>
                              </li>

                          </ul>
                      </div>';
			}
			elseif ($_SESSION['id_level']==2){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>

                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
                        <li>
                            <a href="logout.php"><i class="icon-chevron-right"></i> Keluar</a>
                        </li>

                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==3){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="logout.php"><i class="icon-chevron-right"></i> Keluar</a>
                        </li>


                    </ul>
                </div>';
			}
            ?>

               <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Di Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="index.php">Dashboard</a> <span class="divider">/</span>
	                                    </li>
	                                    <li class="active">Peminjaman</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>


 <div class="right_col" role="main">

     <div class="">
         <div class="page-title">


             <div class="title_right">
                 <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                     <div class="input-group">

                         <span class="input-group-btn">

             </span>
                     </div>
                 </div>
             </div>
         </div>
         <div class="clearfix"></div>

         <div class="row">
             <div class="col-md-12 col-sm-12 col-xs-12">
                 <div class="x_panel">

                     <div class="x_content">

                       <br>
                       <form action="simpan_data.php" method="post" class="form-horizontal row-fluid">
                        

                           <div class="control-group">
                            <label class="control-label" for="basicinput">Id Inventaris</label>
                            <div class="controls">
                              <input name="id_inventaris" type="text" id="basicinput" placeholder="" class="span8" required="" />
                            </div>
                          </div>

                          <div class="control-group">
                           <label class="control-label" for="basicinput">Nama Barang</label>
                           <div class="controls">
                             <input name="nama" type="text" id="basicinput" placeholder="" class="span8" required="" />
                           </div>
                         </div>

                         <div class="control-group">
                          <label class="control-label" for="basicinput">kondisi Barang</label>
                          <div class="controls">
                            <input name="kondisi" type="text" id="basicinput" placeholder="" class="span8" required="" />
                          </div>
                        </div>

                        <div class="control-group">
                         <label class="control-label" for="basicinput">Keterangan Barang</label>
                         <div class="controls">
                           <input name="keterangan" type="text" id="basicinput" placeholder="" class="span8" required="" />
                         </div>
                       </div>

                       <div class="control-group">
                        <label class="control-label" for="basicinput"> jumlah Barang</label>
                        <div class="controls">
                          <input name="jumlah" type="text" id="basicinput" placeholder="" class="span8" required="" />
                        </div>
                      </div>

                      <div class="control-group">
                       <label class="control-label" for="basicinput"> Nama Jenis</label>
                 <div class="col-md-6 col-sm-6 col-xs-12">

                     <div class="controls">
                     <select name="id_jenis" type="text" id="basicinput" placeholder="" class="span8" required="" />
                       <?php
                       include 'koneksi.php';
                       $query = mysql_query("SELECT * from jenis");
                       while($d = mysql_fetch_array($query)){
                           echo $d['id_jenis'];
                           echo "<option value=$d[id_jenis]>$d[nama_jenis]</option>";
                              }      ?>
                     </select>
                 </div>
               </div>
             </div>

             <div class="control-group">
              <label class="control-label" for="basicinput"> Tanggal register</label>
              <div class="controls">
                <input name="tanggal_register" type="date" id="basicinput" placeholder="" class="span8" required="" />
              </div>
            </div>


            <div class="control-group">
             <label class="control-label" for="basicinput"> Ruangan barang</label>
         <div class="col-md-6 col-sm-6 col-xs-12">
           <div class="controls">
           <select name="id_ruang" type="number" id="basicinput" placeholder="" class="span8" required="" />
             <?php
             include 'koneksi.php';
             $query = mysql_query("SELECT * from ruang");
             while($d = mysql_fetch_array($query)){
                 echo $d['id_ruang'];
                 echo "<option value=$d[id_ruang]>$d[nama_ruang]</option>";
                    }      ?>
           </select>
       </div>
         </div>
     </div>

     <div class="control-group">
      <label class="control-label" for="basicinput"> Kode Inventaris</label>
      <div class="controls">
        <input name="kode_inventaris" type="number" id="basicinput" placeholder="" class="span8" required="" />
      </div>
    </div>

    <div class="control-group">
     <label class="control-label" for="basicinput"> Nama Petugas</label>
 <div class="col-md-6 col-sm-6 col-xs-12">
   <div class="controls">
   <select name="id_petugas" type="number" id="basicinput" placeholder="" class="span8" required="" />
     <?php
     include 'koneksi.php';
     $query = mysql_query("SELECT * from petugas");
     while($d = mysql_fetch_array($query)){
         echo $d['id_petugas'];
         echo "<option value=$d[id_petugas]>$d[nama_petugas]</option>";
            }      ?>
   </select>
 </div>
 </div>
 </div>

                             <div class="ln_solid"></div>
                             <div class="form-group">
                                 <div class="col-md-6 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Simpan</button>
                                   <button type="reset" class="btn btn-danger">Reset</button>
                                 </div>
                             </div>
                         </form>

                     </div>
                 </div>
             </div>
         </div>
     </div>
                <div>
                          <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">

                          <thead>
                           <tr>
                             <th>No</th>
                             <th>Nama</th>
                             <th>kondisi</th>
                             <th>keterangan</th>
                             <th>jumlah</th>
                             <th>Nama Jenis</th>
                             <th>tanggal register</th>
                             <th>Nama Ruang</th>
                             <th>kode Inventaris</th>
                             <th>Nama Petugas</th>
                             <th>Aksi</th>
                           </tr>
                          </thead>
                          <tbody>

                          <?php
                                         include "koneksi.php";
                                         $no=1;
                                         $select=mysql_query("select * from inventaris join jenis on inventaris.id_jenis = jenis.id_jenis JOIN
                                         ruang ON inventaris.id_ruang = ruang.id_ruang join petugas ON inventaris.id_petugas = petugas.id_petugas");
                                         while($data=mysql_fetch_array($select))
                                         {
                                         ?>
                                           <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $data['nama']; ?></td>
                          <td><?php echo $data['kondisi']; ?></td>
                          <td><?php echo $data['keterangan']; ?></td>
                          <td><?php echo $data['jumlah']; ?></td>
                          <td><?php echo $data['nama_jenis']; ?></td>
                          <td><?php echo $data['tanggal_register']; ?></td>
                          <td><?php echo $data['nama_ruang']; ?></td>
                          <td><?php echo $data['kode_inventaris']; ?></td>
                          <td><?php echo $data['nama_petugas']; ?></td>


                           <td>
                           <a class="btn btn outline btn-primary fa fa-edit" href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>">Edit</a>
                           <a class="btn btn outline btn-danger fa fa-trash-o" href="hapus.php?id_inventaris=<?php echo $data['id_inventaris']; ?>">Hapus</a>
                           </td>




                                             </tr>
                          <?php
                          }
                          ?>

                          </tbody>
                          </table>
                        </div>

                <?php
                  include ("footer.php");
                 ?>
