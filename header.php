
<?php
include ("cek.php");
 ?>
 <?php
include ("cek_level.php");
  ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>inventaris</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="inventaris.php" class="site_title"><i class="fa fa-paw"></i> <span> Inventaris</span></a>
                    </div>
                    <div class="clearfix"></div>



                    <!-- sidebar menu -->

                    <div class="wrapper">
                        <div class="container">
                                <?php
                                if ($_SESSION['id_level']==1){
                                     echo'<div class="span3" id="sidebar">
                                        <ul class="widget widget-menu unstyled">
                                            <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard</a></li>
                                            <li class="active"><a href="inventaris.php"><i class="menu-icon icon-dashboard"></i>Inventaris</a></li>
                                            <li><a href="peminjaman.php"><i class="menu-icon icon-bullhorn"></i>Peminjaman </a></li>
                                            <li><a href="pengembalian.php"><i class="menu-icon icon-inbox"></i>Pengembalian</a></li>
                                            <li><a href="ruang.php"><i class="menu-icon icon-tasks"></i>Ruang</a></li>
                                        </ul>
                                        <!--/.widget-nav-->


                                        <ul class="widget widget-menu unstyled">
                                            <li><a href="pegawai.php"><i class="menu-icon icon-bold"></i> Pegawai</a></li>
                                            <li><a href="jenis.php"><i class="menu-icon icon-book"></i>Jenis </a></li>
                                        </ul>
                                </div>';
                            }
                                    elseif ($_SESSION['id_level']==2){
                                     echo'<div class="span3" id="sidebar">
                                        <ul class="widget widget-menu unstyled">
                                            <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                            </a></li>
                                            <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
                                            </a></li>
                                            <li><a href="pengembalian.php"><i class="menu-icon icon-bullhorn"></i>Pengembalian</a>
                                            </li>
                                        </ul>
                                </div>';
                            }
                                   elseif ($_SESSION['id_level']==3){
                                     echo'<div class="span3" id="sidebar">
                                        <ul class="widget widget-menu unstyled">
                                            <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                            </a></li>
                                            <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
                                            </a></li>

                                        </ul>
                                </div>';
                            }
                            ?>
                          </div>
                        </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">

                        <a href="logout.php" data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

              <!-- start: Header Menu -->
              <div class="nav-no-collapse header-nav">
                <ul class="nav pull-right">

                  <!-- start: Notifications Dropdown -->

                  <!-- end: Notifications Dropdown -->
                  <!-- start: Message Dropdown -->


                  <!-- start: User Dropdown -->
                    <li class="dropdown">

                    <a href="#" > </i>  <?php echo $_SESSION['petugas'] ?> <i class=""></i>

                    </a>
                    <ul class="dropdown-menu">
                      <li class="dropdown-menu-title">
                        <span>Account Settings</span>
                      </li>

                      <li><a href="login.php"><i class="halflings-icon off"></i> Logout</a></li>
                    </ul>
                  </li>
                  <!-- end: User Dropdown -->
                </ul>
              </div>
              <!-- end: Header Menu -->

            </div>
            <!-- /top navigation -->


            <!-- page content -->
