-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 24 Mar 2019 pada 02.22
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom_alief`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jumlah`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 2, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `kondisi` varchar(25) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` float NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`) VALUES
(1, 'Laptop', 'Baik', 'Fullset', 82, 1, '2019-03-23 13:53:15', 2, 'ABC-0001', 1),
(2, 'Speaker', 'Bagus', 'Baik', 195, 1, '2019-03-23 13:52:23', 2, 'ABC-0002', 3),
(3, 'Headshet', 'Baru', 'Baik', 98, 1, '2019-03-23 13:05:53', 2, 'ABC-0003', 1),
(4, 'Keyboard', 'Bagus', 'Nominus', 98, 1, '2019-03-23 13:53:42', 2, 'ABC-0004', 1),
(5, 'Mouse', 'Sangat Bagus', 'Fullnomin', 80, 1, '2019-03-23 13:41:58', 2, 'ABC-0005', 1),
(6, 'Infocus', 'Bagus', 'Banyak', 200, 1, '2019-03-06 03:21:10', 3, 'ABC-0006', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(25) NOT NULL,
  `kode_jenis` varchar(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Elektronik', 'ABC-0001', 'Fullset');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'Administrator'),
(2, 'Operator'),
(3, 'Peminjam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(25) NOT NULL,
  `nip` int(11) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(1, 'Alief Juan Aprian', 1121323234, 'Jl.Veteran gg Kepatihan 04/01'),
(2, 'Sueb', 2424311, 'Kreteg'),
(3, 'Bagas', 1122434, 'Ciherang'),
(4, 'Mukidi', 121213232, 'jalan yuk'),
(5, 'Burhan', 1123245, 'Pagelaran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime DEFAULT NULL,
  `status_peminjaman` varchar(20) NOT NULL DEFAULT 'Dipinjam',
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(17, '2019-03-23 19:12:21', '2019-03-23 20:44:55', 'Sudah Dikembalikan', 1),
(20, '2019-03-23 20:05:56', '2019-03-23 20:06:02', 'Sudah Dikembalikan', 1),
(21, '2019-03-23 20:42:01', '2019-03-23 20:42:07', 'Sudah Dikembalikan', 1),
(22, '2019-03-23 20:45:09', '2019-03-23 20:51:31', 'Sudah Dikembalikan', 1),
(23, '2019-03-23 20:51:49', '2019-03-23 20:51:54', 'Sudah Dikembalikan', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman_detail`
--

CREATE TABLE `peminjaman_detail` (
  `id` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `jumlah` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman_detail`
--

INSERT INTO `peminjaman_detail` (`id`, `id_inventaris`, `id_peminjaman`, `jumlah`) VALUES
(22, 1, 16, '2'),
(23, 2, 17, '4'),
(24, 1, 18, '2'),
(25, 3, 19, '100'),
(26, 3, 20, '2'),
(27, 5, 21, '20'),
(28, 1, 22, '5'),
(29, 1, 23, '5');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(35) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `id_level`) VALUES
(1, 'Administrator', '12345', 'Alief', 1),
(2, 'Operator', 'kenapa123', 'Juan', 2),
(3, 'Peminjam', 'apasilo', 'Aprian', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` varchar(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Lab-1', 'R-01', 'Fullset'),
(2, 'Lab-2', 'R-02', 'Nominus'),
(3, 'Ruang TU', 'R-03', 'Baik'),
(4, 'Perpustakaan', 'R-04', 'Banyak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `smtr_detail`
--

CREATE TABLE `smtr_detail` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `smtr_detail`
--

INSERT INTO `smtr_detail` (`id_detail_pinjam`, `id_inventaris`, `jumlah_pinjam`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_peminjaman`
--

CREATE TABLE `temp_peminjaman` (
  `id` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `temp_peminjaman`
--

INSERT INTO `temp_peminjaman` (`id`, `id_inventaris`, `id_pegawai`, `jumlah`) VALUES
(14, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `view`
--

CREATE TABLE `view` (
  `id` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(110) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `view`
--

INSERT INTO `view` (`id`, `deskripsi`, `gambar`) VALUES
(2, '<p>SMK Negeri 1 Ciomas mulai beroperasional pada Tahun Pelajaran 2008/2009. Menginjak usianya yang ketiga pada Tahun Pelajaran 2011/2012, SMK Negeri 1 Ciomas telah membuka 2(dua) bidang keahlian dan 3(tiga) bidang keahlian yaitu Bidang Keahlian Teknik Otomotif program keahlian Teknik Kendaraan Ringan (TKR), serta Bidang Keahlian Teknik Inforamasi dan Komunikasi dengan Program Keahlian Rekayasa Perangkat Lunak (RPL) dan Animasi. Lalu pada tahun pelajaran 2015/2016 dibuat jurusan baru yaitu Teknik Pengelasan(TPL).</p>\r\n\r\n<p>Luas area yang telah disetujui Kepala Dinas Pendidikan Kabupaten Bogor seluruhnya kurang lebih 6.500 meter kubik. Area ini berbentuk persegi, namun ada bagian tanah masyarakat yang masih menjorok ke bagian dalam lokasi sekolah (seluas 745 meter kubik). Bagian utara, barat dan selatan berbatasan langsung dengan tanah masyarakat, sementara bagian timur yang merupakan bagian depan SMK menghadap ke jalan Raya Laladon.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Alamat</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;Jl. Desa Laladon No.21 Kecamatan Ciomas Kabupaten Bogor</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Telepon</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;(0251) 7520933</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Web</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;smkn1ciomas.com</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;smkn1_ciomas@yahoo.co.id</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Kode Pos</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;16610</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '1.jpg'),
(4, '<p>Rekayasa Perangkat Lunak Di indonesia dijadikan disiplin ilmu yang dipelajari mulai tingkat Sekolah Menengah Kejuruan sampai tingkatan Perguruan Tinggi. Di tingkat SMK, jurusan ini sudah memiliki kurikulum materi pelajaran sendiri yang sudah ditentukan oleh Dinas Pendidikan.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>Saat Ini Jurusan RPL adalah jurusan yang paling banyak muridnya dari pada jurusan lain di SMK Negeri 1 Ciomas. Termasuk kami, kami di SMKN 1 Ciomas adalah jurusan RPL.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>Kepala Program Jurusan Saat ini adalah Pak Hasrul Adipura Harahap S.Kom, Yang mengajar tentang bahasa pemrograman C++ dan Java Script. Guru mapel Produktif RPL Lainnya ialah Pak Heru Setiawan S.Kom, yang mengajar tentang web dinamis dan juga database mysql. Ada guru lainnya seperti Pak Avian Sofiansyah,Pa Gatot Imam S.Kom, Pak Erwan Usmawan S.Kom.</p>\r\n', '2.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD KEY `id_inventaris` (`id_inventaris`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `peminjaman_detail`
--
ALTER TABLE `peminjaman_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_inventaris` (`id_inventaris`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_level` (`id_level`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `smtr_detail`
--
ALTER TABLE `smtr_detail`
  ADD KEY `id_inventaris` (`id_inventaris`);

--
-- Indexes for table `temp_peminjaman`
--
ALTER TABLE `temp_peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `view`
--
ALTER TABLE `view`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `peminjaman_detail`
--
ALTER TABLE `peminjaman_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `temp_peminjaman`
--
ALTER TABLE `temp_peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `view`
--
ALTER TABLE `view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `smtr_detail`
--
ALTER TABLE `smtr_detail`
  ADD CONSTRAINT `smtr_detail_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
