<?php
include ('cek.php');
include ('koneksi.php');
?>
<!DOCTYPE html>
<html class="no-js">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>inventaris Sekolah</title>
    <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
        rel='stylesheet'>
</head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i>
                                <?php
								echo $_SESSION['petugas'];

                              ?>
                                </a>

                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>

                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
<div class="container-fluid">
            <div class="row-fluid">
            <?php

            if ($_SESSION['id_level']==1){

                      echo'<div class="span3" id="sidebar">
                          <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                              <li>
                                  <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                              </li>
                              <li>
                                  <a href="inventaris.php"><i class="icon-chevron-right"></i> inventaris</a>
                              </li>
                              <li>
                                  <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman </a>
                              </li>
                              <li>
                                  <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian </a>
                              </li>
                              <li>
                                  <a href="laporan.php"><i class="icon-chevron-right"></i> Laporan</a>
                              </li>
                              <br>
                              <li>
                                  <a href="ruang.php"><i class="icon-chevron-right"></i> ruang</a>
                              </li>
                              <li>
                                  <a href="pegawai.php"><i class="icon-chevron-right"></i> Pegawai</a>
                              </li>
                              <li>
                                  <a href="jenis.php"><i class="icon-chevron-right"></i> jenis</a>
                              </li>
                              <li>
                                  <a href="logout.php"><i class="icon-chevron-right"></i> Keluar</a>
                              </li>

                          </ul>
                      </div>';
			}
			elseif ($_SESSION['id_level']==2){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>

                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
                        <li>
                            <a href="logout.php"><i class="icon-chevron-right"></i> Keluar</a>
                        </li>

                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==3){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="logout.php"><i class="icon-chevron-right"></i> Keluar</a>
                        </li>


                    </ul>
                </div>';
			}
            ?>

               <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Di Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="index.php">Dashboard</a> <span class="divider">/</span>
	                                    </li>
	                                    <li class="active">Peminjaman</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>

                

                        <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Table Peminjaman</div>
                            </div>

                            <div class="block-content collapse in">
                                <div class="span12">

									<br>

                            <div class="block-content collapse in">
                                <div class="span12">
                            <div class="block-content collapse in">
                                <div class="span12">

                                <div class="panel-body">
                                    <div class="table-responsive">
            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Pinjam</th>
                            <th>Tanggal Kembali</th>
                            <th>Status Peminjaman</th>
                            <th>Pegawai</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
        include 'koneksi.php';
        $no = 1;
        $select = mysql_query("select * from peminjaman JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai");
        while($data = mysql_fetch_array($select)){
            ?>

                        <tr class="success">
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $data['tanggal_pinjam'] ?></td>
                            <td><?php echo $data['tanggal_kembali'] ?></td>
                            <td><?php echo $data['status_peminjaman'] ?></td>
                            <td><?php echo $data['nama_pegawai'] ?></td>
                            <td><a class="btn btn-info " href="edit_pengembalian.php?id=<?php echo $data['id'] ?>">Kembalikan</a>
                            <a type="button" class="btn btn-danger fa fa-trash" href="hapus_pengembalian.php?id=<?php echo $data['id'] ?>"><b>X</b></a></td>
                        </tr>
                        <?php
        }
        ?>
                </table>
                <br><br>
                <h2>Tabel Data Peminjaman Detail</h2>
            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="example2">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
        include 'koneksi.php';
        $no = 1;
            $select = mysql_query("select peminjaman_detail.jumlah, inventaris.nama, peminjaman_detail.id_peminjaman from peminjaman_detail
                JOIN inventaris ON peminjaman_detail.id_inventaris = inventaris.id_inventaris JOIN peminjaman ON peminjaman_detail.id_peminjaman = peminjaman.id");
        while($data = mysql_fetch_array($select)){
            ?>

                        <tr class="success">
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $data['nama'] ?></td>
                            <td><?php echo $data['jumlah'] ?></td>
                            <td>
                            <center><a type="button" class="btn btn-danger fa fa-trash" href="hapus_peminjaman_detail.php?id=<?php echo $data['id'] ?>"><b>X</b></a></td></center>
                        </tr>
                        <?php
        }
        ?>
                </table>
                <script type ="text/javascript" src="assets/js/jquery.min.js"></script>
                                        <script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
                                        <script>$(document).ready(function(){
                                            $('#example').DataTable();
                                        });
                                        </script>
                                                        <script type ="text/javascript" src="assets/js/jquery.min.js"></script>
                                        <script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
                                        <script>$(document).ready(function(){
                                            $('#example2').DataTable();
                                        });
                                        </script>

            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

                     <div class="row-fluid">
                        <!-- block -->

                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <center>
                    <p>&copy; Inventory Sekolah@ 2019</p>
                </center>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {

        });
        </script>
    </body>

</html>
