<?php

include ('koneksi.php');

if(!isset($_SESSION)){
	session_start();
}

if(isset($_POST['username'])){
	$username = mysql_real_escape_string(addslashes(trim($_POST['username'])));
	$password = mysql_real_escape_string(trim($_POST['password']));

	$query = mysql_query("SELECT * FROM petugas WHERE username = '$username' AND password = '$password'");
	$data = mysql_fetch_array($query);
	$cek=mysql_num_rows($query);
	if ($cek>0)
	{
		$_SESSION['petugas'] = $username;
		$_SESSION['id_level'] =  $data['id_level'];
		header("location:successlogin.php");
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>inventaris Sekolah</title>
	<link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link type="text/css" href="css/theme.css" rel="stylesheet">
	<link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
	<link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
			rel='stylesheet'>
</head>

<body style="background:#F7F7F7;">

    <div class="">

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form align="center" action="" method="post" class="form-signin">
                        <h1>Login Form</h1>
                        <div>
                            <input type="text" pattern="[A-Za-z]{8-0}" name="username" class="form-control" placeholder="Username" maxlength="30" required="" />
                        </div>
                        <div>
                            <input type="password" name="password" class="form-control" placeholder="Password" maxlength="10" required="" />
                        </div>
                        <div>
                          <button class="btn btn-default submit"  type="submit" value="login" id="login-button" >Login</button>
                          <?php
                  				if (isset($cek) && (!$cek))
                  				{
                  					echo 'Login gagal, username atau password salah<br><br>';
                  				}
                  				?>

                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">


                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Inventaris Login</h1>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>

        </div>
    </div>

</body>

</html>
